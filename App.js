/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';  
import { Alert, AppRegistry, Button, StyleSheet, View, Image } from 'react-native';  
import EscPos from "@leesiongchan/react-native-esc-pos";
//import ImagePicker from 'react-native-image-picker';


const design = `
D0004           {<>}           Table #: A1
------------------------------------------
[ ] Espresso
    - No sugar, Regular 9oz, Hot
                              {H3} {R} x 1
------------------------------------------
[ ] Blueberry Cheesecake
    - Slice
                              {H3} {R} x 1

{QR[Where are the aliens?]}
{IMG[file://apimobile-devel.baresto.id/api/print/receipt/kot/sP4626WyF3IqxZcWHNLSHgEgHhVyQ6.png}
`;

export default class App extends Component {  
    onPressButton = async() => {  
      try {
        // Can be `network` or `bluetooth`
        EscPos.setConfig({ type: "network" });
    
        // Connects to your printer
        // If you use `bluetooth`, second parameter is not required.
        await EscPos.connect("192.168.100.13", 9100);
    
        // Once connected, you can setup your printing size, either `PRINTING_SIZE_58_MM` or `PRINTING_SIZE_80_MM`
        EscPos.setPrintingSize(EscPos.PRINTING_SIZE_80_MM);
        // 0 to 8 (0-3 = smaller, 4 = default, 5-8 = larger)
        EscPos.setTextDensity(8);
        // Test Print
        //await EscPos.printSample();
        // Cut half!
        //await EscPos.cutPart();
        // You can also print image!
        //await EscPos.printImage(file.uri); // file.uri = "file:///longpath/xxx.jpg"
        // Print your design!
        await EscPos.printDesign(design);
        // Print QR Code, you can specify the size
        //await EscPos.printQRCode("Proxima b is the answer!", 200);
        // Cut full!
        await EscPos.cutFull();
        // Beep!
        await EscPos.beep();
        // Kick the drawer! Can be either `kickCashDrawerPin2` or `kickCashDrawerPin5`
        //await EscPos.kickCashDrawerPin2();
        // Disconnect
        await EscPos.disconnect();
      } catch (error) {
        console.error(error);
      }
      Alert.alert('You clicked the button!')  
    }  
    

    render() {  
        let Image_Http_URL ={ uri: 'https://i.pinimg.com/564x/4a/dd/d5/4addd5336a51540aadb8e3ecec431ce3.jpg'};
        
        return (  
          <View style={styles.container}>  
            <View style={styles.buttonContainer}>  
              <Button  
                  onPress={this.onPressButton}  
                  title="Press Me"  
              />  
            </View>  
          </View>
        );  
    }  
}  
  
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
}); 